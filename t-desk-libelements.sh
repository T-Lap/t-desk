#!/bin/bash
xdg_open() {
  open="$(ls -a | fzf $fthem )"
  xdg-open "$open" &
}
exit-t() {
      printf "\n"
      line_to "Press q again to EXIT" 9
      printf "\n"
      read -rsn1 -p "EXIT>" quit
      if [[ "$quit" == q ]]; then clear -x ; exit; else startpage;fi
}

pwd_line() {
  line_to "$(pwd)" "$accent"
}

disk-space() {
df -h | sed -e '/tmpfs/d' | sed -e '2i-------------------------------------------------' | sed 's/^/             /g'
}

dir-down() {
  dir="$(ls -a | fzf $ftheme )"
  cd "$dir"
}
