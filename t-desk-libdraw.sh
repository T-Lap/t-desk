#!/bin/bash
#
#
# Drawing Functions
# =========
# Draw text centered and give you control over background and foreground color.
# line_to "your text" <bg-color num> <fg-color num> "*"
line_to() {
  TEXT="$1"
	SYM="${4:- }"
	COLOR="$3"
  BGCOLOR="$2"
	declare -i COLS="$(tput cols)"
	declare -i TEXT_LENG="${#TEXT}"
	declare -i LENG=$(( ($COLS - $TEXT_LENG) / 2 - 1 ))
	[[ -n $COLOR ]] && tput setaf $COLOR
	[[ -n $BGCOLOR ]] && tput setab $BGCOLOR
	for ((i = 0 ; i < LENG ; i++)); do
	    printf "$SYM"
	done
	printf " $TEXT " 
	for ((i = 0 ; i < LENG ; i++)); do
	    printf "$SYM"
	done
	[[ $(($LENG*2+$TEXT_LENG+2)) -ne $COLS ]] && printf "$SYM"	
	tput sgr 0
	return
}
# draw a line with text centered and control over the fg-color.
#line_t "text" <color-num> <symbol>
line_t() {
  TEXT="$1"
	SYM="${3:-━}"
	COLOR="$2"
	declare -i COLS="$(tput cols)"
	declare -i TEXT_LENG="${#TEXT}"
	declare -i LENG=$(( ($COLS - $TEXT_LENG) / 2 - 1 ))
	[[ -n $COLOR ]] && tput setaf $COLOR
	for ((i = 0 ; i < LENG ; i++)); do
	    printf "$SYM"
	done
	printf " $TEXT "
	for ((i = 0 ; i < LENG ; i++)); do
	    printf "$SYM"
	done
	[[ $(($LENG*2+$TEXT_LENG+2)) -ne $COLS ]] && printf "$SYM"	
	tput sgr 0
	return
}
tblock() {
  offset="$1"
  leng=""
  text="$2"
	sym="${5:- }"
	color="$4"
  bg="$3"
  echo -e "$text" | while read -r l; do
    [[ "$leng" -lt "${#l}" ]] && leng="$((${#l}+2))"
  done
  echo -e "$text" | while read -r line;do
    printf "%*s" $offset "$sym"
    [[ -n $color ]] && tput setaf $color
    [[ -n $bg ]] && tput setab $bg
    printf "%-*s\n" "$leng" "$line" 
    tput sgr 0
	done
	return
}
hr() {
  range=$(seq 1 $(($COLUMNS/8*6)))
  fill=$(for i in $range ; do echo -n "━"; done)
  tblock "$(($COLUMNS/8))" "$fill" "16" "$accent"
}
th1() {
  headerText="$@"
  tblock $(($COLUMNS/8)) "$headerText" "$accent"
  printf "\n\n"
}
th2() {
  headerText="$@"
  tput bold
  tput setaf "$accent"
  tblock $(($COLUMNS/8)) "$headerText"
  tput sgr0
  printf "\n"
}
th3() {
  headerText="$@"
  tput bold
  tblock $(($COLUMNS/8)) "$headerText"
  tput sgr0
  printf "\n"
}
tp() {
  ptext="$(echo $@ | fold -w $(($COLUMNS/2)) -s)"
  echo -e "$ptext" | while read -r line;do
  tblock $(($COLUMNS/8)) "$line"
  done 
  printf "\n"
}
quote() {
  ptext="$(echo $@ | fold -w $(($COLUMNS/8*6)) -s)"
  echo -e "$ptext" | while read -r line;do
  tblock $(($COLUMNS/8)) "█ $line"
  done 
  printf "\n"
}
tlist() {
  line="$(echo $@ | fold -w $(($COLUMNS/8*6)) -s)"
  tblock $(($COLUMNS/8)) "• $line"
}
th1c() {
  headerText="$@"
  line_to "$headerText" "$accent"
  printf "\n\n"
}
th2c() {
  headerText="$@"
  tput bold
  line_t "$headerText" "$accent" " "
  tput sgr0
  printf "\n"
}
th3c() {
  headerText="$@"
  tput bold
  line_to "$headerText"
  tput sgr0
  printf "\n"
}
tpc() {
  paragraph="$(echo $@ | fold -w $(($COLUMNS/8*6)) -s)"
  echo -e "$paragraph" | while read -r line;do
  line_to "$line"
  done 
  printf "\n"
}

column_12() {
  list="$@"
  newline="1"
  echo -e "$list" | while read l;do
    if [[ "$newline" -eq "1" ]]; then
      printf "\n" 
      tput dim sc
      tput cuf "$((COLUMNS/8))"
      printf "$l"
      newline="0"
    else
      tput rc
      tput cuf "$((COLUMNS/8*4))"
      printf "  $l"
      newline="1"
    fi
  done
}

column_1x() {
  list="$@"
  limit="$(($(printf "$list" | wc -l)/2))"
  newline=1
  printf "$list" | while read l;do
    if [[ "$newline" -le "$limit" ]]; then
      printf "\n" 
      tput dim
      tput cuf "$((COLUMNS/8))"
      printf "$l"
      if [[ $newline -eq "$limit" ]]; then
        printf "\n"
        tput cuu "$limit"
      fi
      newline=$newline+1
    else
      tput dim
      tput cuf "$((COLUMNS/8*4))"
      printf "  $l\n"
      newline="$(($newline+1))"
    fi
  done
}

column_11() {
  list="$(echo $1 | fold -w $(($COLUMNS/4)) -s)"
  list2="$(echo $2 | fold -w $(($COLUMNS/4)) -s)"
  tput sc
  printf "$list" | while read l;do
      tput dim
      tput cuf "$((COLUMNS/8))"
      printf "$l\n"
  done
  tput rc
  printf "$list2" | while read l;do
      tput dim
      tput cuf "$((COLUMNS/8*4))"
      printf "  $l\n"
  done
}
right_block() {
  text="$1"
  bcolor="${2:-accent}"
  leng="$(($COLUMNS/8*2))"
  offset="$(($COLUMNS/8*6))"
  tput cub $COLUMNS
  tput cuf $offset
  tput setab $bcolor
  printf "  %-*s" "$leng" "$text" 
  tput sgr0
}
ltor_block() {
  text="$1"
  bcolor="${2:-accent}"
  leng="$(($COLUMNS/8*6))"
  offset="$(($COLUMNS/8*2))"
  tput cub $COLUMNS
  tput cuf $offset
  tput setab $bcolor
  printf "  %-*s" "$leng" "$text" 
  tput sgr0
}
left_block() {
  text="$1"
  bcolor="${2:-accent}"
  leng="$(($COLUMNS/8*2))"
  tput setab $bcolor
  printf "%*s \n" "$leng" "$text" 
  tput sgr0
}
rtol_block() {
  text="$1"
  bcolor="${2:-accent}"
  leng="$(($COLUMNS/8*6))"
  tput setab $bcolor
  printf "%*s \n" "$leng" "$text" 
  tput sgr0
}
