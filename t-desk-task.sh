#!/bin/bash
#
source ~/.local/lib/lib-tuikit
#
task() {
  taskspace="${HOME}/.config/t-desk/t-task"
  [ ! -e "$taskspace/config" ] && mkdir -p $taskspace/ ; touch $taskspace/config
  line_to "T-TASK" 6
  list_command task_menu
  read
}

task
