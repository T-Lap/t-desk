#!/bin/bash -
#===============================================================================
#
#          FILE: t-desk-bookmarks.sh
#
#         USAGE: ./t-desk-bookmarks.sh
#
#   DESCRIPTION: 
#
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: YOUR NAME (), 
#  ORGANIZATION: 
#       CREATED: 01/11/2023 03:47:05 PM
#      REVISION:  ---
#===============================================================================
bookmark() {
  # bmsource="${1:=bmlist}"
  # check for bm file
  bmlist="$(cat ~/.local/share/t-desk/bmlist | sort )"
  # create case list
  # draw page
  printf "\n"
  th3c "T-Desk-Bookmarks"
  hr
  column_12 "$bmlist"
  printf "\n"
  read -rsn2 bm
  blink="$(echo "$bmlist" | grep -e "^$bm " )"
  cd "${blink:3}"
  # echo "$bmlist" | while read -r line;do
  #   [[ "$bm" == "${line:0:2}" ]] && cd "${line:3}" 
  #   echo "${line:3}"
  # done
  # goto="$(echo "$sclist" | grep "$bm" | cut -d " " -f 1)"
  startpage
}

