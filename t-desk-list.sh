#!/bin/bash
# 
list_command() {
  list="${1:-main_menu}"
  com_list=`sed 's/::.*$//g' ~/.local/share/t-desk/$list`
  printf "\n"
  read -rsn1 -p "$list>" bm
  blink="$(grep -e "^$bm " ~/.local/share/t-desk/$list)"
  run=$(echo "${blink:4}" | sed 's/^.*:://')
  eval "$run"
  list_command "$list"
}
list_no_option() {
     printf "\n"
     line_to " $(echo $bm) - Is not an option" 13
     printf "\n"
}
