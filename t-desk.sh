#!/bin/bash -
#===============================================================================
#
#          FILE: cli-desk-temp.sh
#
#         USAGE: 
#
#   DESCRIPTION: Creating a desk app for your terminal
#
#       OPTIONS: ---
#  REQUIREMENTS: bash, tput
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Thomas Pautler (), 
#  ORGANIZATION: T-Lap
#       CREATED: 01/07/2023 10:15:37 AM
#      REVISION:  ---
#=====================================================================
#
#=====================================================================
# Set global vars
# export CD_ACCENT_COLOR="5"
#=====================================================================
# Set local vars
title="CLI-DESK"
accent=236
color=16
ftheme="--height=60% --reverse --border=rounded --margin=0,10%"
#=====================================================================
#
export TD_ACCENT="$accent"
#
#
#=====================================================================
# Import basic functions
#
source /home/thomas/.local/share/t-desk/t-desk-libelements.sh
source /home/thomas/.local/share/t-desk/t-desk-libdraw.sh
source /home/thomas/.local/share/t-desk/t-desk-bookmarks.sh
source /home/thomas/.local/share/t-desk/t-desk-list.sh

#======================================================================
# Elements
#
#the nav function is a template. just copy and rename to nav-XXX..
#
# nav() {
#   escape_char=$(printf "\u1b")
#   read -rsn1 mode # get 1 character
#   if [[ $mode == $escape_char ]]; then
#       read -rsn4 -t 0.001 mode # read 2 more chars
#   fi
#   case $mode in
#     [A | 'k' ) # up
#       nav ;;
#     [C | 'j' ) # down
#       nav ;;
#     [B | 'l' ) # right
#       nav ;;
#     [D | 'h' ) # left
#       nav ;;
#     q) return ;;
#     * ) nav ;;
#   esac
# }
#
trun() {
  tput el1
  read -p "run: " run
  eval $run
}

quicknote() {
  canvaso
  line_to "last notes" 5
  newest=$(ls ~/.notes/By_Date/ | tail -n 1)
  cat ~/.notes/By_Date/$newest | sed '/..:..:../i =========================================='
  echo
  tput dim
  line_t " n/new - o/open - v/note_from_clipboard "
  echo
  list_command note-menu
}

fzfemoji() {
  # emojis=$(cat ~/.local/share/emoji)
  chosen=$(cat ~/.local/share/emoji | fzf $ftheme | awk '{print $1}')
  [ -z "$chosen" ] && startpage
  if [ -n "$1" ]; then
    xdotool type "$chosen"
  else
    printf "$chosen" | xclip -selection clipboard
    printf "\n\n"
    line_to " $chosen Copied " "$accent"
  fi
}

fman() {
    man -k . | fzf -q "$1" --prompt='man> ' --margin 4%  --preview $'echo {} | tr -d \'()\' | awk \'{printf "%s ", $2} {print $1}\' | xargs -r man' | tr -d '()' | awk '{printf "%s ", $2} {print $1}' | xargs -r man
}

lbash() {
line_to "${USER}  T-DESK   $(date '+%a %d %b %Y   %H:%M')" $accent
}

nav-sp() {
list_command
}

sclist-nav-sp() {
column_1x "$(sed 's/::.*$//g' ~/.local/share/t-desk/main_menu)"
  tput sgr0
  printf "\n"
  nav-sp
}

canvas() {
  clear -x
  lbash
  # add basic functions bottom dim
  tput cup $LINES 0
  line_to "?/short cuts - t/terminal - q/quit" $accent
  tput cup 2 0
}
canvaso() {
  clear -x
  lbash
  tput cup 2 0
}
addbm() {
  right_block "add bookmark" 8
  read -p "XX character for your bookmark: " bmin
  [[ "$bmin" == "q" ]] && startpage
  ex=$(grep -r "^$bmin " /home/thomas/.local/share/t-desk/bmlist)
  if [[ -n "$ex" ]]; then
    line_to "$ex already exits" 9
  else
    echo "$bmin $(pwd)" >> /home/thomas/.local/share/t-desk/bmlist
    line_to "shortcut created" 2
  fi
}
# app runner
runhtop() {
  htop
  startpage
}
#======================================================================
#pages


setcolor() {
  canvas
  th3c "Setup your Environment color"
  hr
  for i in {1..256};do tput setab $i ; echo -n " $i " ;done
  tput sgr0
  echo " "
  th3 "Your current color is $accent" $accent
  th3 Chose a accent color number:
  read ac
  canvas
  if [[ -z "$ac" ]];then
    th2c "Color not changed" "1"
    sleep 2
    startpage
    else
      accent="$ac" && th2c "Color change suggested" "2"
      sleep 1
      startpage
  fi
}

startpage() {
  canvas
  top="${1:-lsd}"
  eval "$top"
  ## to slow on home can be function
  ##dsize="$(du -hs $(pwd))"
  ##size="$(echo $dsize | sed 's/ .*$//')"
  ##right_block "size: $size" $accent
  ##
  printf "\n"
  pwd_line
  printf "\n"
  nav-sp
}

#=====================================================================
# Main operation
#
sleep 0.2
startpage 
